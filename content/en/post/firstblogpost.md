---
date: 2017-04-14T11:25:05-04:00
title: "CS371p Fall 2022: Emma Reuter"
disable_share: false
---

![headshot](https://media-exp1.licdn.com/dms/image/C5603AQE7peiwQAllqg/profile-displayphoto-shrink_400_400/0/1640645130902?e=1667433600&v=beta&t=V09HycTwYLool_Wcw9NdCNroXdVZxjzNHpjP_HJUYXQ)

Hi, I'm Emma. I'm from Chicago originally and as a midwestern gal I love food. I have very strong opinions about hot dogs! I'm a fifth year student so High school feels far away, but my favorite extracurricular was robotics. I developed a strong love for computer science as I learned how to program the school's robot. I came to UT for it's computer science program. I was also tired of Chicago's infamously cold winters. I have taken software engineering with Professor Downing. From my expercience with SWE I know this class will be a lot of work but I will get a lot of it. I used C++ at my internship this summer. We were debuginng a large C++ codebase so I spent a lot of time reading C++ code and looking at gdb stack traces. I would say I'm fammiliar with C++ and C but I don't know everything. I liked the first lecture. I already knew how Docker works so that portion of the lecture was a refresher for me. Seeing all my friends coming back from summer break made me happy this week. My pick of the week is [pwn.college](https://pwn.college). It's a course from ASU teaching you about binary exploitation. It's been really interesting working my way through the course. Knowing more about binary exploitation will help you in a software engineering role. 
