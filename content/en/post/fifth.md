---
date: 2022-10-9T11:25:05-04:00
title: "CS371p Fall 2022: Emma Reuter"
disable_share: false
---

![headshot](https://media-exp1.licdn.com/dms/image/C5603AQE7peiwQAllqg/profile-displayphoto-shrink_400_400/0/1640645130902?e=1667433600&v=beta&t=V09HycTwYLool_Wcw9NdCNroXdVZxjzNHpjP_HJUYXQ)

## What did you do this past week?

This last week I took a chem test, it went ok. I also turned in my object oriented programming voting project. I also wrote problems for the ISSS ctf this past friday. ISSS hosts capture the flag competitions every other friday join the [discord](https://isss.io/discord) for more info.

## What's in your way?

I still have chem homework to do. I also have an offer deadline expiring soon.

## What will you do next week?

Next week I will turn in my ethical hacking homework. I will also also watch Monday's and Wednesday's object oriented programming classes since I missed those. 


## What did you think of Paper #7: The Liskov Substitution Principle?

I thought it was interesting way of explaining how child objects should be able to replace their parents without change the application. I am however, tired of reading programming papers that use shapes as an example for object oriented design. Lets be more creative.

## What was your experience of std::array() and std::vector? (this question will vary, week to week)

I like how std::array and std::vector did the memory management for us so we didn't have to call new and delete. 

## What made you happy this week?

I'm happy I got through the chem test and the project. Also people liked my capture the flag problem for the ISSS ctf which made me feel good. It was suid problem where ssh-keygen had its suid bit set. The solution involved writing a shared library downloading the shared library onto the problem machine and then loading the library using the -D flag for ssh-keygen.

## What's your pick-of-the-week or tip-of-the-week?

My pick of the week is VSCode. I like using vscode as my primary text editor because it has so many extensions! It has extensions to automatically check the syntax of c++. It also has extensions for Makefiles, Dockerfiles and python files. I'm also a fan of the dark theme. The dark theme is easier on the eyes and makes coding for hours easier.