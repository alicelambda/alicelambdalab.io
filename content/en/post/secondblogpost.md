---
date: 2022-10-30T11:25:05-04:00
title: "CS371p Fall 2022: Emma Reuter"
disable_share: false
---

![headshot](https://media-exp1.licdn.com/dms/image/C5603AQE7peiwQAllqg/profile-displayphoto-shrink_400_400/0/1640645130902?e=1667433600&v=beta&t=V09HycTwYLool_Wcw9NdCNroXdVZxjzNHpjP_HJUYXQ)


# What did you do this past week?
This past week I took a chem test. I finished my allocator project. I also finished a government assigment. 

# What's in your way?

Nothing's in my way at the moment.

# What will you do next week?

Next week I will start on the darwin project. I will also catch up on missed OOP lectures. I'm also throwing a late halloween party for my friends which should be fun.

# What did you think of Paper 10. Why getter and setter methods are evil?

I thought it made a strong argument that getter and setters are poor design choices.

# What was your experience of iteration and initializations? (this question will vary, week to week)
My experience of iteration is that it can be very useful to define your own iterators. I also thought it was a cool idea that you could create const iterators that don't allow you to modify the underlying data. I didn't find iterator to be particularly helpful for the Allocator project. My iterator was defined as pointing to the first sentinel of each block. This implementation wasn't useful enough for me to use in my allocate and deallocate methods. 

# What made you happy this week?

I got brunch with a friend I hadn't seen in a while. Also I caught up on schoolwork! I didn't think I would be able to finish Allocator in time but then I did. 🤠 I also tried out a new recipe and it turned out delicious.

# What's your pick-of-the-week or tip-of-the-week?

My tip of the week is drawing diagrams. I found draws diagrams on paper was particularly helpful when working out the implementation denials of the allocator project. 