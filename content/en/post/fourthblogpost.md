---
date: 2017-04-14T11:25:05-04:00
title: "CS371p Fall 2022: Emma Reuter"
disable_share: false
---

![headshot](https://media-exp1.licdn.com/dms/image/C5603AQE7peiwQAllqg/profile-displayphoto-shrink_400_400/0/1640645130902?e=1667433600&v=beta&t=V09HycTwYLool_Wcw9NdCNroXdVZxjzNHpjP_HJUYXQ)

## What did you do this past week?

This past week I wrote my first government paper. I also worked on my object oriented programming voting project. I also wrote capture the flag problems for the Information and Systems Security Society. My problems were all [suid](https://www.redhat.com/sysadmin/suid-sgid-sticky-bit) problems. That required hackers to use programs in novel ways to get the flag. I completed an ethical hacking homework. As well as multiple chemistry homeworks. 

## What's in your way?

I wasn't able to register early enough to take my chemistry exam with accommodations. So I will have to take the exam without accommodations. This is super frustrating to me because part of my disability is poor executive function. I also have a job offer expiring soon and I'm not sure whether or not I want to take it. Recruiting on top of classwork is exhausting.

## What will you do next week?

Next week my I will sit my second chemistry exam. I will also finish up my object oriented programming project. 

## What did you think of Paper #6: The Open-Closed Principle?

I haven't read the open closed principle yet.

## What was your experience of equals(), templates, and iterators? (this question will vary, week to week)

I liked the abstraction of iterators it is intuitive and makes sense to me. I found templates to be confusing because I'm used to languages where you can specify what functions your type requires but with templates you can't. Implementing equals was straightforward. 

## What made you happy this week?

This week has been pretty grueling but I'm happy that it is over. My brother finished his months long hiking trip across California and I'm glad that he enjoyed it. 

## What's your pick-of-the-week or tip-of-the-week?

My pick of the week is mynoise.net. It's a site that generates background noise that I find helpful to listen to during programming project. I like the Irish Coast, Cafe Restaurant, Rain On A Tent, and Distant Thunder generators. They also offer super generator which allows you to combine multiple generators into a soundscape. If you're like me and get distracted easily you may find it helpful.