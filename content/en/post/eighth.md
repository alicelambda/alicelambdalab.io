---
date: 2022-11-13T11:25:05-04:00
title: "CS371p Fall 2022: Emma Reuter"
disable_share: false
---

![headshot](https://avatars.githubusercontent.com/u/42192226?s=400&u=be077c1e865465e293ce055d1de0bb5a1c126d1c&v=4)

# What did you do this past week?

This past week I worked on my OOP project. Also my org the Information & Systems Security Society had Security day. I worked on my chemistry homework. I studied for an upcoming government exam.  

# What's in your way?

I'm competing with HASH, UT's competitive cybersecurity team, next weekend. So I need to make sure I'm on top of everything academically. The competition is penetration testing competition and we will be competing in Augusta Georgia. Penetration testing is where you hack computers and then document how you broke in to improve their security. 

# What will you do next week?

Competing in the Collegiate Penetration Testing Competition. I'm excited to bond with my team and hopefully go on to nationals! I will also be studying for chemistry because I have chemistry test coming up. 


# What did you think of Paper #12. Why extends is evil?

Extends is definitely evil. It was interesting that the author recommended against using interface inheritance because that was technique that was introduced to us in 314. 

# What was your experience of function overloading, move, and vector? (this question will vary, week to week)

I thought move was pretty cool. It allows us to right more performant code by avoiding unnecessary copying. 

# What made you happy this week?
The cold! It made me nostalgic for my hometown of Chicago. I also modified my single speed bike to be a fixed gear. It has been fun and exciting to learn how to ride it with its new fixed wheel configuration. I've noticed I have more control of the speed of the bike by using my legs as resistance. I want to learn how bike backwards.

# What's your pick-of-the-week or tip-of-the-week?
Tip of the week is to start by writing the input parser for OOP programming projects. For this project I got overwhelmed by the number of things there was to do so picking a concrete starting point earlier on would have been helpful! 