---
date: 2022-11-06T11:25:05-04:00
title: "CS371p Fall 2022: Emma Reuter"
disable_share: false
---

![headshot](https://avatars.githubusercontent.com/u/42192226?s=400&u=be077c1e865465e293ce055d1de0bb5a1c126d1c&v=4)

# What did you do this past week?
This past week I wrote capture the flag problems for the Information & Systems Security Society. I studied chemistry. I also started running again, which has been fun. I had a late halloween party which is fun! I registered for classes for the last time which was exciting. I'm taking compilers, natural language processing and english next semester! I also hope to start studying for cybersecurity certifications next semester with my reduced course load. 

# What's in your way?

I need to review for my coming chemistry final. I also need to work on Darwin. 

# What will you do next week?

Next week my main goal is to finish Darwin. I will also start reviewing for my chemistry final.  

# What did you think of Paper 11. More on getters and setters?
I found the paper to be confusing. Especially the part explaining how to write user interfaces without using getters or setters.

# What was your experience of initializer_list and vector? (this question will vary, week to week)
I found initializer_list to be helpful when creating more complex objects. I found the vector exercise to be challenging. I didn't realize there was a difference between uncommenting the test cases and clicking the button that says run test cases. 


# What made you happy this week?
I had dinner with my cousin and got to catch up with her. I also got to hangout with friends. I was happy to be finally caught up on school work as well.

# What's your pick-of-the-week or tip-of-the-week?

My pick of the week is Obsidian. Obsidian is a graph based note taking editor. I find helpful to make links between my notes. Links are important for seeing how different pieces of information relate to each other. I've been using Obsidian to organize my ethical hacking notes. 